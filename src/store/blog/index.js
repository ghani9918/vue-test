import actions from './actions'
import mutations from './mutations'
import getters from './getters'

const state = {
    article: []
}
const module = {
    namespaced: true,
    state,
    actions,
    mutations,
    getters,
}

export default module  
import axios from 'axios'
const actions = {
    async getDataArticleApi({ commit } ){
        let { data } = await axios.get('https://jsonplaceholder.typicode.com/posts')    
        var data2=[]
        for(var i=80; i<data.length;i++)
        {
            data2= [...data2,data[i]]
        }
        commit('allArticleget', data2)
    },
    async generateDataArticle({ commit }, payload ){
        axios.post('https://jsonplaceholder.typicode.com/posts', {
            title: payload.title,
            body: payload.body,
            userId:payload.userId
          })
          .then(function (response) {
              commit('addArtilce', payload)
          })
          .catch(function (error) {
          });

    },
    removeDataArticle({ commit }, index){
        const data = index
        commit('removingArticle', data)
    },
    updateDataArticle({ commit }, [ ArticleId, payload ] ){
        commit('UpdateArticle', [ ArticleId, payload ] )
    },
}
export default actions